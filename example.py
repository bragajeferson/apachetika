import tika
tika.initVM()
from tika import parser as tparser

def lerArquivoComTika(folder):
    try:            
        parsed = tparser.from_file(folder)
        if parsed['status'] == 415:
            raise ValueError("Tipo de midia não suportado")
        elif parsed['status'] == 200:
            if parsed["content"] is not None:
                read_data = parsed["content"]
            else:
                print("Erro ao ler o conteúdo")
    except (ValueError, Exception) as e: 
        print(e)